Attribute VB_Name = "PRCconnection"
Global DBgQueryCount: Global DBsQueryCount: Global isProduction
Dim objMyConn, objMyCmd, objMyRecordset, objFields As Object

Private Function FUNsetDBObjects()
Set objMyConn = CreateObject("ADODB.Connection")
Set objMyCmd = CreateObject("ADODB.Command")
Set objMyRecordset = CreateObject("ADODB.Recordset")
End Function

Private Function UPDDBConnection()

Update(1, 1) = 20170330: Update(1, 2) = "Created and send out"
Update(2, 1) = 20170524: Update(2, 2) = "Added SetSQLData For Insert/Update SP Multiple Per Connection"
Update(3, 1) = 20170525: Update(3, 2) = "Allowed SqlStatement To Be Variable/Array"
Update(4, 1) = 20170623: Update(4, 2) = "Created new function 'DATget' and 'DATset' that allows to switch users and databases and get header or not"
Update(5, 1) = 20170725: Update(5, 2) = "Added cQueryCount and automatic debug.print to DATget function"
Update(6, 1) = 20171030: Update(6, 2) = "Added rocky as a new connection. changed the error handler for DATset (connection error) and (insert error)"
Update(7, 1) = 20180202: Update(7, 2) = "Added FUNswithToProduction"
Update(8, 1) = 20180515: Update(8, 2) = "Objects Updated to late binding"

End Function

Public Function FUNswithToProduction(database)

'************************************************************************************
'Switch between databases based on the isProduction boolean
If isProduction = False Then
    Select Case database
        Case "hiqtrading_p", "hiqtrading_r"
        FUNswithToProduction = "hiqtrading_t"
        Case "hiqadmin"
        FUNswithToProduction = "hiqadmin_t"
        Case "backoffice_p"
        FUNswithToProduction = "backoffice_t"
        Case Else: FUNswithToProduction = database
    End Select
    
    Else
    FUNswithToProduction = database
End If
'************************************************************************************

'************************************************************************************
'Temporarily move all queries from replication to production on a specific date
'If Date = "10/11/2018" And database = "hiqtrading_r" Then
'    FUNswithToProduction = "hiqtrading_p"
'End If
'************************************************************************************

End Function

Public Function FUNgetDatabase(database) As Variant

Dim loginDetails(1 To 4) As Variant

'************************************************************************************
'Get login details for each database
If database = "hiqtrading_p" Then
    loginDetails(1) = "192.168.120.125": loginDetails(2) = "hiqtrading_p": loginDetails(3) = "merijn.dirks": loginDetails(4) = "meridik"
ElseIf database = "hiqadmin" Then
    loginDetails(1) = "192.168.120.125": loginDetails(2) = "hiqadmin": loginDetails(3) = "merijn.dirks": loginDetails(4) = "meridik"
ElseIf database = "backoffice_p" Then
    loginDetails(1) = "192.168.120.133": loginDetails(2) = "backoffice_p": loginDetails(3) = "merijn.dirks": loginDetails(4) = "Wohz+o4sow"
ElseIf database = "hiqtrading_r" Then
    loginDetails(1) = "a-slave01.internal.degiro.eu": loginDetails(2) = "hiqtrading_p": loginDetails(3) = "merijn.dirks": loginDetails(4) = "meridik"
ElseIf database = "hiqtrading_t" Then
    loginDetails(1) = "192.168.20.77": loginDetails(2) = "hiqtrading_t": loginDetails(3) = "root": loginDetails(4) = "Rocky|V"
ElseIf database = "backoffice_t" Then
    loginDetails(1) = "192.168.20.77": loginDetails(2) = "backoffice_t": loginDetails(3) = "root": loginDetails(4) = "Rocky|V"
ElseIf database = "hiqadmin_t" Then
    loginDetails(1) = "192.168.20.77": loginDetails(2) = "hiqadmin_t": loginDetails(3) = "root": loginDetails(4) = "Rocky|V"
End If
'************************************************************************************

FUNgetDatabase = loginDetails

End Function

Public Function DATget(sqlStatement, header, database)

    setObject = FUNsetDBObjects()
    loginDetails = FUNgetDatabase(database)
    Dim tempSqlData()
    DBgQueryCount = DBgQueryCount + 1
    
    On Error GoTo error_handler
    
    If IsArray(sqlStatement) = True Then
        Dim el, SqlString As String
        SqlString = ""
        For Each el In sqlStatement
            SqlString = SqlString & CStr(el)
        Next
    Else
        SqlString = sqlStatement
    End If

    Debug.Print "Query Get" & DBgQueryCount & ":     " & SqlString
    
    'Open Connection
    Dim pwd As String
    objMyConn.ConnectionString = _
          "DRIVER={MySQL ODBC 5.2 ANSI Driver};" & "SERVER=" & loginDetails(1) & ";" & "DATABASE=" & loginDetails(2) & ";" & "UID=" & loginDetails(3) & ";" & "PWD=" & loginDetails(4) & ";" & "OPTION=3"
    objMyConn.Open

    'Set and Excecute SQL Command'
    Set objMyCmd.ActiveConnection = objMyConn
    objMyCmd.CommandText = SqlString: objMyCmd.CommandType = adCmdText
           
    Set objMyRecordset.Source = objMyCmd
    objMyRecordset.Open
    Set objFields = objMyRecordset.Fields
    countfields = objFields.Count
    ReDim sqlNAME(0 To countfields - 1)

    Sql = objMyRecordset.GetRows

    If header = True Then
            ReDim tempSqlData(LBound(Sql) To UBound(Sql), -1 To UBound(Sql, 2))
            
            For iHeader = LBound(Sql) To UBound(Sql)
            tempSqlData(iHeader, -1) = objFields.Item(iHeader).Name
                For iRow = LBound(Sql, 2) To UBound(Sql, 2)
                tempSqlData(iHeader, iRow) = Sql(iHeader, iRow)
                Next iRow
            Next iHeader
        Else
            tempSqlData = Sql
    End If
    DATget = tempSqlData
    Set objMyConn = Nothing: Set objMyCmd = Nothing
    
error_handler:
Set objMyConn = Nothing: Set objMyCmd = Nothing

End Function

Public Function DATset(sqlStatement, InsertMessage, database)
    
    loginDetails = FUNgetDatabase(database)
    setObject = FUNsetDBObjects()
    objMyConn.ConnectionString = _
          "DRIVER={MySQL ODBC 5.2 ANSI Driver};" & "SERVER=" & loginDetails(1) & ";" & "DATABASE=" & loginDetails(2) & ";" & "UID=" & loginDetails(3) & ";" & "PWD=" & loginDetails(4) & ";" & "OPTION=3"
    
    On Error GoTo connection_error
    objMyConn.Open

    On Error GoTo error_handler
    
    If FUNdimensionCheck(sqlStatement) > 0 Then
        For i = LBound(sqlStatement) To UBound(sqlStatement)
            DBsQueryCount = DBsQueryCount + 1
            Debug.Print "   Query Set " & DBsQueryCount & ":     " & sqlStatement(i)
            Set objMyRecordset = objMyConn.Execute(sqlStatement(i))
        Next i
        
        Else
        
        Debug.Print "   Query Set " & DBsQueryCount & ":     " & sqlStatement
        Set objMyRecordset = objMyConn.Execute(sqlStatement)
    End If
    
    objMyConn.Close
    
    If InsertMessage <> False Then
        If FUNdimensionCheck(sqlStatement) > 0 Then
        InsertDone = FUNsetMessageBoxWithTimer(UBound(sqlStatement) - LBound(sqlStatement) + 1 & " " & InsertMessage)
        Else
        InsertDone = FUNsetMessageBoxWithTimer(InsertMessage)
        End If
    Else
    If FUNdimensionCheck(sqlStatement) > 0 Then
        DATset = UBound(sqlStatement) - LBound(sqlStatement) + 1
        Else
        DATset = 1
    End If
    End If
    
    Set objMyConn = Nothing: Set objMyCmd = Nothing
    Exit Function
    
connection_error:
Debug.Print "Cannot connect to database" & vbnewlines & "Server=" & loginDetails(1) & ";" & "DATABASE=" & loginDetails(2) & ";" & "UID=" & loginDetails(3) & ";" & "PWD=" & loginDetails(4) & ";" & "OPTION=3"
Exit Function
     
error_handler:
    If FUNdimensionCheck(sqlStatement) > 0 Then
    Debug.Print "ERROR while inserting on line " & i & " out of " & UBound(sqlStatement) - LBound(sqlStatement) + 1 & vbNewLine & "Line: " & sqlStatement(i)
    Else
    Debug.Print "ERROR while inserting on line 1 out of 1 " & vbNewLine & "Line: " & sqlStatement
    End If

Set objMyConn = Nothing: Set objMyCmd = Nothing

End Function

Public Function DATset_return(sqlStatement, database)
    
    Dim gatheredIDs()
    
    '//This is function is intended to be used for stored procedure only. In the stored procedure the last inserted id is returned using "select last_insert_id();"
    setObject = FUNsetDBObjects()
    loginDetails = FUNgetDatabase(FUNswithToProduction(database))
    Dim tempSqlData()
    DBgQueryCount = DBgQueryCount + 1
    
    On Error GoTo error_handler
    
    Dim pwd As String

    objMyConn.ConnectionString = _
          "DRIVER={MySQL ODBC 5.2 ANSI Driver};" & "SERVER=" & loginDetails(1) & ";" & "DATABASE=" & loginDetails(2) & ";" & "UID=" & loginDetails(3) & ";" & "PWD=" & loginDetails(4) & ";" & "OPTION=3"
    objMyConn.Open
    Set objMyCmd.ActiveConnection = objMyConn

    If FUNdimensionCheck(sqlStatement) > 0 Then
        ReDim gatheredIDs(LBound(sqlStatement) To UBound(sqlStatement))
      
        For i = LBound(sqlStatement) To UBound(sqlStatement)
        Debug.Print "   Query Set " & DBsQueryCount & ":     " & sqlStatement(i)
        objMyCmd.CommandText = sqlStatement(i): objMyCmd.CommandType = adCmdText
        Set objMyRecordset.Source = objMyCmd
        objMyRecordset.Open
        Sql = objMyRecordset.GetRows
        gatheredIDs(i) = Sql(0, 0)
        objMyRecordset.Close
        Next i
    End If

    Set objMyConn = Nothing: Set objMyCmd = Nothing
    DATset_return = gatheredIDs
error_handler:
Set objMyConn = Nothing: Set objMyCmd = Nothing

End Function







