Option Explicit

Dim sqlProduct() As Variant
Dim valuation_date As Date
Dim fund_group As String

Private m_rhistoryManager As RHistoryAPI.RHistoryManager
Private m_rhistoryCookie As Long
Private WithEvents m_rhistoryQuery As RHistoryAPI.RHistoryQuery
Private Declare Function CreateReutersObject Lib "PLVbaApis.dll" (ByVal progID As String) As Object

Public Sub PRCreuters_getdata(select_fund_group As String)

Dim sqlDates() As Variant
Dim iColumn As Integer, i_reutersid As Integer
Dim iRow As Long
Dim reuters_string As String

fund_group = select_fund_group

If m_rhistoryManager Is Nothing Then 'Create reuters object
    Set m_rhistoryManager = CreateReutersObject("RHistoryAPI.RHistoryManager")
    m_rhistoryCookie = m_rhistoryManager.Initialize("MY BOOK")
End If
If m_rhistoryQuery Is Nothing Then
    Set m_rhistoryQuery = m_rhistoryManager.CreateHistoryQuery(m_rhistoryCookie)
Else
    m_rhistoryQuery.Cancel
End If
    
Call MTHget_sql_data(sqlDates(), sqlProduct()) 'Get position overview

If FUNcheckForEmptiness(sqlProduct) = False Then
    
    For iColumn = LBound(sqlProduct) To UBound(sqlProduct)
        If sqlProduct(iColumn, LBound(sqlProduct, 2)) = "ric_code" Then i_reutersid = iColumn
    Next iColumn
    
    For iRow = LBound(sqlProduct, 2) + 1 To UBound(sqlProduct, 2) 'Create instrumentlist
    reuters_string = reuters_string & sqlProduct(i_reutersid, iRow) & ";"
    Next iRow
    
    m_rhistoryQuery.InstrumentIdList = reuters_string 'List of RIC codes, separated by semicolon
    m_rhistoryQuery.FieldList = "TRDPRC_1.TIMESTAMP;TRDPRC_1.OPEN;TRDPRC_1.CLOSE;" 'List of fields separated by semicolon [TRDPRC_1.TIMESTAMP;TRDPRC_1.OPEN;TRDPRC_1.CLOSE]
    m_rhistoryQuery.RequestParams = "INTERVAL:1D START:" & CStr(Format(DateAdd("d", -5, sqlDates(0, 1)), "yyyy-mm-dd")) & " END " & CStr(Format(sqlDates(0, 1), "yyyy-mm-dd"))     'Time frame where start is day before [INTERVAL:1D START:YYYY-MM-DD END YYYY-MM-DD]
    m_rhistoryQuery.RefreshParams = ""
    m_rhistoryQuery.DisplayParams = "CH:In;Fd" 'Display parameters default [CH:In;Fd]
    m_rhistoryQuery.Subscribe
End If
    
End Sub

Private Function MTHget_sql_data(sqlDates() As Variant, sqlProduct() As Variant)

sqlDates = DATget("SELECT valuation_date FROM fa_nav WHERE fund_id in (" & FUNfundIdFundGroup(fund_group) & ") GROUP BY valuation_date ORDER BY valuation_date DESC LIMIT 2", _
        False, "hiqtrading_p")
        
valuation_date = sqlDates(0, 0)

Call FUNsetMessageBoxWithTimer("Started Price Check " & fund_group & " for valuation_date= " & valuation_date)
        
sqlProduct = DATget("SELECT fa_fund.name AS fund_name, IFNULL(reuters_id,isin) AS ric_code, " & _
                "IF(hiqkey LIKE 'DU%',CASE WHEN SUBSTRING_INDEX(reuters_id,'.',-1) IN ('T','HK','AX','SI') THEN DATE(creation_time) ELSE valuation_date END, IF(continent_code IN ('OC','AS'), DATE(creation_time),valuation_date)) AS pricing_date, " & _
                "IF(hiqkey LIKE 'DU%',CASE WHEN SUBSTRING_INDEX(reuters_id,'.',-1) IN ('T','HK','AX','SI') THEN 'OPEN' ELSE 'CLOSE' END,IF(continent_code IN ('OC','AS'),'OPEN','CLOSE')) AS oc_indicator, " & _
                "product_id, isin AS isin_code, " & _
            "alphabetic_code AS currency, valuation_source AS price_source, valuation_price/10000 AS nav_price, '' AS reuters_price, '' AS message FROM fa_navproductdata " & _
            "LEFT JOIN currency USING (currency_id) LEFT JOIN fa_nav USING (id) LEFT JOIN fa_fund ON (fa_fund.id=fund_id) LEFT JOIN product USING (product_id) LEFT JOIN exchangeinfo USING (exchange_id) " & _
            "WHERE fund_id in (" & FUNfundIdFundGroup(fund_group) & ") AND valuation_date='" & CStr(Format(valuation_date, "yyyy-mm-dd")) & "' AND contract_type=1;", _
        True, "hiqtrading_p")

End Function

Private Sub m_rhistoryQuery_OnImage(ByVal a_historyTable As Variant)

Dim iDBcolumn As Byte
Dim iDBrow As Integer
Dim iRHcolumn As Integer
Dim iRHrow As Byte

Dim iRic_code As Byte, iPricing_date As Byte, iReuters_price As Byte, iOc_indicator As Byte, iMessage As Byte, iNav_price As Byte 'declare column numbers from database

Dim ric_code As String
Dim oc_indicator As String
Dim pricing_date As Date

Dim iRHcolumn_ocindicator As Integer
Dim iRHcolumn_timestamp As Integer
Dim iRHrow_date As Byte

For iDBcolumn = LBound(sqlProduct, 1) To UBound(sqlProduct, 1) 'Search for column numbers in report
    If sqlProduct(iDBcolumn, LBound(sqlProduct, 2)) = "ric_code" Then iRic_code = iDBcolumn
    If sqlProduct(iDBcolumn, LBound(sqlProduct, 2)) = "pricing_date" Then iPricing_date = iDBcolumn
    If sqlProduct(iDBcolumn, LBound(sqlProduct, 2)) = "reuters_price" Then iReuters_price = iDBcolumn
    If sqlProduct(iDBcolumn, LBound(sqlProduct, 2)) = "oc_indicator" Then iOc_indicator = iDBcolumn
    If sqlProduct(iDBcolumn, LBound(sqlProduct, 2)) = "message" Then iMessage = iDBcolumn
    If sqlProduct(iDBcolumn, LBound(sqlProduct, 2)) = "nav_price" Then iNav_price = iDBcolumn
Next iDBcolumn


For iDBrow = LBound(sqlProduct, 2) + 1 To UBound(sqlProduct, 2)
    
    iRHcolumn_ocindicator = 0
    iRHcolumn_timestamp = 0
    iRHrow_date = 0
    
    If IsNull(sqlProduct(iRic_code, iDBrow)) = True Then GoTo nextiDBrow 'check if symbol exists otherwise skip this line
    
    ric_code = sqlProduct(iRic_code, iDBrow)
    If sqlProduct(iOc_indicator, iDBrow) = "OPEN" Then oc_indicator = "Trade Open" Else oc_indicator = "Trade Close"
    pricing_date = sqlProduct(iPricing_date, iDBrow)
    
    For iRHcolumn = LBound(a_historyTable, 2) To UBound(a_historyTable, 2) 'Get correct column for ric_code and oc_indicator match
        If a_historyTable(LBound(a_historyTable), iRHcolumn) = ric_code And a_historyTable(LBound(a_historyTable) + 1, iRHcolumn) = oc_indicator Then iRHcolumn_ocindicator = iRHcolumn
        If a_historyTable(LBound(a_historyTable), iRHcolumn) = ric_code And a_historyTable(LBound(a_historyTable) + 1, iRHcolumn) = "Timestamp" Then iRHcolumn_timestamp = iRHcolumn
    Next iRHcolumn
    
    For iRHrow = LBound(a_historyTable, 1) To UBound(a_historyTable, 1) 'Get correct row for correct timestamp column and pricing date match
        If a_historyTable(iRHrow, iRHcolumn_timestamp) = pricing_date Then iRHrow_date = iRHrow
    Next iRHrow
    
    If iRHcolumn_ocindicator <> 0 And iRHrow_date <> 0 Then 'If values are unequal default values then fill in price
        sqlProduct(iReuters_price, iDBrow) = a_historyTable(iRHrow_date, iRHcolumn_ocindicator)
    End If
    
nextiDBrow:

    If sqlProduct(iReuters_price, iDBrow) = "" Then 'Generate message based on matching info
        
        For iRHrow = LBound(a_historyTable, 1) + 1 To UBound(a_historyTable, 1)
            If a_historyTable(iRHrow, iRHcolumn_timestamp) < pricing_date Then
            
                If sqlProduct(iNav_price, iDBrow) = a_historyTable(iRHrow, iRHcolumn_ocindicator) Then
                sqlProduct(iMessage, iDBrow) = "Found Matching Price for Old Pricing Date (" & a_historyTable(iRHrow, iRHcolumn_timestamp) & ")"
                Else
                    If IsDate(a_historyTable(iRHrow, iRHcolumn_ocindicator)) = True Then
                    sqlProduct(iMessage, iDBrow) = "Could not find match (" & a_historyTable(iRHrow, iRHcolumn_timestamp) & ")"
                    Else
                    sqlProduct(iMessage, iDBrow) = "Found Mismatch with Old Pricing Date (" & a_historyTable(iRHrow, iRHcolumn_timestamp) & ")"
                    End If
                End If
                If IsDate(a_historyTable(iRHrow, iRHcolumn_ocindicator)) = True Then
                sqlProduct(iReuters_price, iDBrow) = ""
                Else
                sqlProduct(iReuters_price, iDBrow) = a_historyTable(iRHrow, iRHcolumn_ocindicator)
                End If
                Exit For
            End If
        Next iRHrow
     
    ElseIf sqlProduct(iReuters_price, iDBrow) <> sqlProduct(iNav_price, iDBrow) Then
        sqlProduct(iMessage, iDBrow) = "Price mismatch"
    Else
        sqlProduct(iMessage, iDBrow) = "Match"
    End If
    
    If sqlProduct(iReuters_price, iDBrow) = "" Then
        sqlProduct(iMessage, iDBrow) = "Check RIC"
    End If
    
Next iDBrow

Call FUNcreateTxtFile(sqlProduct, "C:\Users\reuters\Desktop\Price Check\Daily Price Checks\", fund_group & " Price Check " & FUNdateString(valuation_date), True, "csv", ";", True) 'Write results to desktop
End Sub

Private Sub m_rhistoryQuery_OnError(ByVal a_status As Long, ByVal a_statusDescription As String)
    Call MsgBox("Error: " & a_statusDescription, vbOKOnly + vbCritical)
End Sub



