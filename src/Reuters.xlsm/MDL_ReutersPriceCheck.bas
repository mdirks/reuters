Attribute VB_Name = "MDL_ReutersPriceCheck"
'{20191010} Changed the way funds are gathered per fund group

Option Explicit

Public Sub start()

Dim credentials() As Variant

'************************************************************************************
'Check File and set isProduction
isProduction = True
credentials = FUNgetDatabase("hiqtrading_p")
If credentials(2) = "hiqtrading_t" Then
    isProduction = False
End If
Call FUNcheckFileVersion("REUT_20191010")
'************************************************************************************

Dim QB_Form As Form_fundgroup
Set QB_Form = New Form_fundgroup
QB_Form.Show

End Sub
