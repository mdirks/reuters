Attribute VB_Name = "PRCfunction"
Private Declare PtrSafe Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As LongPtr) As Long
Private Declare PtrSafe Function CoCreateGuid Lib "ole32" (ByRef GUID As Byte) As Long
Private Declare PtrSafe Sub GetSystemTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)

Private Type SYSTEMTIME
    wMilliseconds As Integer
End Type

Dim objDictionary, objOutlookApplication, objScriptingFileSystem, objWordApplication, objFileDialog, objWScriptShell, objExcelApplication As Object
Global wrdDoc As Object

Private Function FUNsetGFObjects()
Set objDictionary = CreateObject("Scripting.Dictionary")
Set objScriptingFileSystem = CreateObject("scripting.filesystemobject")
Set objWordApplication = CreateObject("Word.Application")
Set objExcelApplication = CreateObject("Excel.Application")
Set objFileDialog = Application.FileDialog(4)
Set objWScriptShell = CreateObject("WScript.Shell")
End Function

Public Function FUNgenerateGUID() As String
    Dim ID(0 To 15) As Byte
    Dim N As Long
    Dim GUID As String
    Dim Res As Long
    Res = CoCreateGuid(ID(0))

    For N = 0 To 15
        GUID = GUID & IIf(ID(N) < 16, "0", "") & Hex$(ID(N))
        If Len(GUID) = 8 Or Len(GUID) = 13 Or Len(GUID) = 18 Or Len(GUID) = 23 Then
            GUID = GUID & "-"
        End If
    Next N
    FUNgenerateGUID = GUID
End Function

Public Function FUNgetReconPath()
FUNgetReconPath = "K:\Recon\"
End Function

Public Function FUNgetFundsharePath()
FUNgetFundsharePath = "K:\Fundshare\"
End Function

Public Function FUNdateString(valuedate) As String
FUNdateString = Format(valuedate, "YYYYMMDD")
End Function

Public Function FUNtimeString(valuedate) As String
FUNtimeString = Format(valuedate, "YYYYMMDD_HHMMSS")
End Function

Public Function FUNreDateString(valuedate) As Date
FUNreDateString = DateSerial(Left(valuedate, 4), Mid(valuedate, 5, 2), Right(valuedate, 2))
End Function

Public Function FUNlastDayOfMonth(year, month)
nextMonth = DateSerial(year, month + 1, 1)
FUNlastDayOfMonth = DateAdd("d", -1, nextMonth)
End Function

Public Function FUNcsvIsOpen(MyFile)

On Error GoTo FileProblem

Open MyFile For Output As #1
Close #1
    FUNcsvIsOpen = False
    Exit Function

FileProblem:
    FUNcsvIsOpen = True

End Function

Public Function FUNcreateTxtFile(data, Location, fileName, Reverse, fileExtension, Delimiter, Quotes)

Dim printLine()

MyFile = Location & fileName & "." & fileExtension

Do While FUNcsvIsOpen(MyFile) = True
fileNumber = fileNumber + 1
MyFile = Location & fileName & "(" & fileNumber & ")" & "." & fileExtension
Loop

If Quotes = True Then quotevalue = Chr(34) Else quotevalue = ""
Open MyFile For Output As #1

If FUNdimensionCheck(data) = 2 Then
    If Reverse = False Then
        ReDim printLine(LBound(data, 1) To UBound(data, 1))
        For i = LBound(data, 1) To UBound(data, 1)
            For j = LBound(data, 2) To UBound(data, 2)
            If j <> UBound(data, 2) Then Separator = Delimiter Else Separator = ""
            printLine(i) = printLine(i) & quotevalue & data(i, j) & quotevalue & Separator
            Next j
            Print #1, printLine(i)
        Next i
    Else
        ReDim printLine(LBound(data, 2) To UBound(data, 2))
        For i = LBound(data, 2) To UBound(data, 2)
            For j = LBound(data, 1) To UBound(data, 1)
            If j <> UBound(data, 1) Then Separator = Delimiter Else Separator = ""
            printLine(i) = printLine(i) & quotevalue & data(j, i) & quotevalue & Separator
            Next j
            Print #1, printLine(i)
        Next i
    End If

Else

    ReDim printLine(LBound(data) To UBound(data))
    For i = LBound(data) To UBound(data)
        printLine(i) = printLine(i) & quotevalue & data(i) & quotevalue & Separator
    Next i
End If

Close #1

Debug.Print "Created file:      " & MyFile
Call FUNsetMessageBoxWithTimer("created file: " & MyFile)

End Function

Public Function FUNcheckForEmptiness(arrayData)

If IsNull(arrayData) = True Or IsEmpty(arrayData) = True Then
        ArrayCheck = True
        FUNcheckForEmptiness = ArrayCheck
    Else
        iDimension = FUNdimensionCheck(arrayData)
End If

Select Case iDimension
    Case 0
        ContentCheck = True
    Case 1
        ContentCheck = True
        For i = LBound(arrayData, 1) To UBound(arrayData, 1)
            If IsNull(arrayData(i)) = False Then
                If IsEmpty(arrayData(i)) = False Then
                ContentCheck = False
                Exit For
                End If
            End If
        Next i
    Case 2
        ContentCheck = True
        For i = LBound(arrayData, 1) To UBound(arrayData, 1)
            For j = LBound(arrayData, 2) To UBound(arrayData, 2)
                If IsNull(arrayData(i, j)) = False Then
                    If IsEmpty(arrayData(i, j)) = False Then
                    ContentCheck = False
                    Exit For
                    End If
                End If
            Next j
        Next i
    Case 3
        ContentCheck = True
        For i = LBound(arrayData, 1) To UBound(arrayData, 1)
            For j = LBound(arrayData, 2) To UBound(arrayData, 2)
                For N = LBound(arrayData, 3) To UBound(arrayData, 3)
                    If IsNull(arrayData(i, j, N)) = False Then
                        If IsEmpty(arrayData(i, j, N)) = False Then
                        ContentCheck = False
                        Exit For
                        End If
                    End If
                Next N
            Next j
        Next i
End Select

FUNcheckForEmptiness = ContentCheck

Exit Function

error_handler:
    If Err.Number <> 0 Then
    FUNcheckForEmptiness = True
    End If

End Function

Public Function FUNstringFiller(stringLine, stringSize, LeftRight)

FUNstringFiller = stringLine

If LeftRight = "Left" Then
    If Len(stringLine) < stringSize Then
        For i = 1 To stringSize - Len(stringLine)
        FUNstringFiller = FUNstringFiller & Chr(32)
        Next i
        Else
        FUNstringFiller = Left(stringLine, stringSize)
    End If
Else
    If Len(stringLine) < stringSize Then
        For i = 1 To stringSize - Len(stringLine)
        TempString = TempString & Chr(32)
        Next i
        FUNstringFiller = TempString & FUNstringFiller
        Else
        FUNstringFiller = Left(stringLine, stringSize)
    End If
End If

End Function

Public Function FUNdimensionCheck(arrayData)

    On Error GoTo Err
    Dim i, tmp As Long
    i = 0
    Do While True
        i = i + 1
        tmp = UBound(arrayData, i)
    Loop
Err:
    FUNdimensionCheck = i - 1

End Function

Public Function FUNcreateUniqueItemsList(DRange As Variant) As Variant

Dim i, j, NumRows, NumCols As Long
Call FUNsetGFObjects

If TypeName(DRange) = "Range" Then DRange = DRange.Value2
NumRows = UBound(DRange)

If NumRows = 0 Then
    Dim temp(1 To 1, 1 To 1)
    temp(1, 1) = DRange(UBound(DRange))
    FUNcreateUniqueItemsList = temp
    Else
    For j = 0 To NumRows
    objDictionary(DRange(j)) = 1
    Next j
    FUNcreateUniqueItemsList = WorksheetFunction.Transpose(objDictionary.keys)
End If

End Function

Public Function FUNtrim(StringValue)

 For j = 1 To Len(StringValue)
    Select Case Asc(Mid(StringValue, j, 1))
        Case 9, 32, 160: leftx = leftx + 1
        Case Else: Exit For
    End Select
 Next j

For j = Len(StringValue) To 1 Step -1
    Select Case Asc(Mid(StringValue, j, 1))
        Case 9, 32, 160: rightx = rightx + 1
        Case Else: Exit For
    End Select
 Next j

FUNtrim = Left(Right(StringValue, Len(StringValue) - leftx), Len(Right(StringValue, Len(StringValue) - leftx)) - rightx)

End Function

Public Function FUNlastClearingOutputDate(ReportingDate)

     clDate = DATget("select date_format(max(date),'%Y%m%d') date from clearingposition where  date<=" & ReportingDate & " order by date desc limit 1;", False, "hiqtrading_r")
     FUNlastClearingOutputDate = clDate(0, 0)

End Function

Public Function FUNlastPositionAttributeDate(ReportingDate)

    paDate = DATget("select date_format(date,'%Y%m%d') date from positionattribute where int_account=60000 and type in (1000,1001) and date<=" & ReportingDate & " order by date desc limit 1", False, "hiqtrading_r")
    FUNlastPositionAttributeDate = paDate(0, 0)

End Function

Public Function FUNcashAccountStatusCash()

    Array_CashAccountStatusCash = DATget("select account_id from cashaccount where status&1=1;", False, "hiqtrading_r")

    For i = LBound(Array_CashAccountStatusCash, 2) To UBound(Array_CashAccountStatusCash, 2)
        If i = UBound(Array_CashAccountStatusCash, 2) Then
        FUNcashAccountStatusCash = FUNcashAccountStatusCash & Array_CashAccountStatusCash(0, i)
        Else
        FUNcashAccountStatusCash = FUNcashAccountStatusCash & Array_CashAccountStatusCash(0, i) & ","
        End If
    Next i

End Function

Public Function FUNcashAccountStatusCashReservation()

    Array_CashAccountStatusCash = DATget("select account_id from cashaccount where (status&1=1 or status=512);", False, "hiqtrading_r")

    For i = LBound(Array_CashAccountStatusCash, 2) To UBound(Array_CashAccountStatusCash, 2)
        If i = UBound(Array_CashAccountStatusCash, 2) Then
        FUNcashAccountStatusCashReservation = FUNcashAccountStatusCashReservation & Array_CashAccountStatusCash(0, i)
        Else
        FUNcashAccountStatusCashReservation = FUNcashAccountStatusCashReservation & Array_CashAccountStatusCash(0, i) & ","
        End If
    Next i

End Function

Public Function FUNprebooktransaction(INTACCOUNT, PRODUCTID, SIZE, PRICE, bookdate, exttradeid, ClearingRoute, transactiontype, correctiontext, caid, reference, username)

FUNprebooktransaction = "call prebook_transaction_user(" & INTACCOUNT & "," & PRODUCTID & "," & SIZE & "," & PRICE & "," & bookdate & ",'" & exttradeid & "','" & ClearingRoute & "'," & transactiontype & ",'" & correctiontext & "'," & caid & ",'" & reference & "','" & username & "');"

End Function

Public Function FUNprebookcashtransaction(PRODUCTID, transactionid, INTACCOUNT, cashaccountid, bookdate, amount, currencyid, caid, ClearingRoute, comment, bookreference, username)

FUNprebookcashtransaction = "call prebook_cashtransaction_user(" & PRODUCTID & "," & transactionid & "," & INTACCOUNT & "," & cashaccountid & "," & bookdate & "," & amount & "," & currencyid & "," & caid & ",'" & ClearingRoute & "','" & comment & "','" & bookreference & "','" & username & "');"

End Function

Public Function FUNfundIdFundGroup(FundGroup) As String

Dim type_string As String
Dim sqlData() As Variant

Select Case FundGroup
    Case "FUNDSHARE"
    type_string = "in (5,6) and manager=2"
    Case "CASHFUND"
    type_string = "in (3) and manager=2"
    Case "ROBEIN"
    type_string = "in (20) and manager=1"
End Select

sqlData = DATget("SELECT GROUP_CONCAT(id) FROM fa_fund WHERE activity_flag=1 AND type " & type_string, _
    False, FUNswithToProduction("hiqtrading_p"))
    
If FUNcheckForEmptiness(sqlData) = False Then
    FUNfundIdFundGroup = sqlData(0, 0)
    Else
    FUNfundIdFundGroup = ""
End If
    
End Function

Public Function FUNuserNameWindows() As String

    Dim lngLen As LongPtr
    Dim strBuffer As String

    Const dhcMaxUserName = 255

    strBuffer = Space(dhcMaxUserName)
    lngLen = dhcMaxUserName
    If CBool(GetUserName(strBuffer, lngLen)) Then
        FUNuserNameWindows = Left$(strBuffer, lngLen - 1)
    Else
        FUNuserNameWindows = ""
    End If

End Function

Public Function FUNsetStyleGood(SheetName, Row, column)

Sheets(SheetName).Cells(Row, column).Font.Italic = True
Sheets(SheetName).Cells(Row, column).Font.Bold = True
Sheets(SheetName).Cells(Row, column).Font.color = RGB(0, 97, 0)
Sheets(SheetName).Cells(Row, column).Interior.color = RGB(198, 239, 206)

End Function

Public Function FUNsetStyleBad(SheetName, Row, column)

Sheets(SheetName).Cells(Row, column).Font.Italic = True
Sheets(SheetName).Cells(Row, column).Font.Bold = True
Sheets(SheetName).Cells(Row, column).Font.color = RGB(156, 0, 6)
Sheets(SheetName).Cells(Row, column).Interior.color = RGB(255, 199, 206)

End Function

Public Function FUNsetStyleCheck(SheetName, Row, column)

Sheets(SheetName).Cells(Row, column).Font.Italic = True
Sheets(SheetName).Cells(Row, column).Font.Bold = True
Sheets(SheetName).Cells(Row, column).Font.color = RGB(255, 255, 255)
Sheets(SheetName).Cells(Row, column).Interior.color = RGB(165, 165, 165)

End Function

Public Function FUNgetWorkingDaysArray(StartDate, EndDate)

Dim dateArray()
ReDim dateArray(1 To 1)

DayCount = DateDiff("d", StartDate, EndDate)

For i = 0 To DayCount
Select Case Weekday(DateAdd("d", -i, EndDate))
    Case 2, 3, 4, 5, 6
    j = j + 1
    dateArray(j) = FUNdateString(DateAdd("d", -i, EndDate))
    If i <> DayCount Then ReDim Preserve dateArray(1 To UBound(dateArray) + 1)
    Case Else
End Select
Next i

FUNgetWorkingDaysArray = dateArray

End Function

Public Function FUNsetMessageBoxWithTimer(message)
    Dim Time As Integer
    Call FUNsetGFObjects

    Time = 1 'Set the message box to close after "time" seconds
    Select Case objWScriptShell.Popup(message, Time, "Message Box", 0)
        Case 1, -1
            Exit Function
    End Select

End Function

Public Function FUNclearingAllFormat(SheetName, StartRow, StartColumn, EndRow, EndColumn)

    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).ClearContents
    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).Borders(xlEdgeBottom).LineStyle = xlNone
    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).Font.SIZE = 10
    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).Font.Italic = False
    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).Font.Bold = False
    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).Font.Underline = False
    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).Interior.color = RGB(255, 255, 255)
    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).Font.color = RGB(0, 0, 0)
    Sheets(SheetName).Range(Cells(StartRow, StartColumn), Cells(EndRow, EndColumn)).UnMerge

End Function

Public Function FUNwindowsToMySql()

WindowUser = FUNuserNameWindows()

Select Case LCase(WindowUser)
    Case "mdirks"
    FUNwindowsToMySql = "merijn.dirks"
    Case "mbouwman"
    FUNwindowsToMySql = "menno.bouwman"
    Case "rsnijders"
    FUNwindowsToMySql = "remco.snijders"
    Case "nstoeva"
    FUNwindowsToMySql = "nevena.stoeva"
    Case "jwaeghe"
    FUNwindowsToMySql = "jasper.waeghe"
    Case "gshahinyan"
    FUNwindowsToMySql = "grisha.shahinyan"
    Case "abeyerer"
    FUNwindowsToMySql = "alex.beyerer"
    Case "tarlt"
    FUNwindowsToMySql = "tomas.arlt"
    Case "tbainbridge"
    FUNwindowsToMySql = "t.bainbridge"
    Case "mpetrova"
    FUNwindowsToMySql = "martina.petrova"
    Case "caltmeyer"
    FUNwindowsToMySql = "c.altmeyer"
    Case "nneudert"
    FUNwindowsToMySql = "nick.neudert"
    Case "nneudert"
    FUNwindowsToMySql = "nick.neudert"
    Case "inikolov"
    FUNwindowsToMySql = "iliyan.nikolov"
    Case "rtocha"
    FUNwindowsToMySql = "ruben.tocha"
    Case "rvduren"
    FUNwindowsToMySql = "roy.duren"
    Case Else
    FUNwindowsToMySql = "Undefined"
End Select

End Function

Public Function FUNapprovalRight()

hasright = DATget("select true from pb_rights where approveright=7 and user_name='" & FUNwindowsToMySql() & "';", False, FUNswithToProduction("hiqtrading_p"))

If FUNcheckForEmptiness(hasright) = False Then
    FUNapprovalRight = True
    Else
    FUNapprovalRight = False
End If

End Function

Public Function FUNcheckApprovalRights(book_user)

If FUNapprovalRight() = False Then
    FUNcheckApprovalRights = -1
    Else
    If book_user = FUNwindowsToMySql() Then
        FUNcheckApprovalRights = 0
        Else
        FUNcheckApprovalRights = 1
    End If
End If

End Function

Function FUNisInArray(stringToBeFound, arrayList)

  FUNisInArray = False

  For i = LBound(arrayList) To UBound(arrayList)
    If StrComp(stringToBeFound, arrayList(i), vbTextCompare) = 0 Then
      FUNisInArray = True
      Exit For
    End If
  Next i

End Function

Function FUNcontainsInArray(stringToBeFound, arrayList)

  FUNcontainsInArray = False

  For i = LBound(arrayList) To UBound(arrayList)
    If InStr(arrayList(i), stringToBeFound) <> 0 Then
      FUNcontainsInArray = True
      Exit For
    End If
  Next i

End Function

Function FUNisInArrayReturn(stringToBeFound, arrayList, Dimension)

  FUNisInArrayReturn = False

  Select Case Dimension
    Case 1
        For i = LBound(arrayList, 1) To UBound(arrayList, 1)
          If StrComp(stringToBeFound, arrayList(i, LBound(arrayList, 2)), vbTextCompare) = 0 Then
            FUNisInArrayReturn = arrayList(i, UBound(arrayList, 2))
            Exit For
          End If
        Next i
    Case 2
        For i = LBound(arrayList, 2) To UBound(arrayList, 2)
          If StrComp(stringToBeFound, arrayList(LBound(arrayList, 1), i), vbTextCompare) = 0 Then
            FUNisInArrayReturn = arrayList(UBound(arrayList, 1), i)
            Exit For
          End If
        Next i
    End Select

End Function

Public Function FUNreversedArray(arrayList)

Dim reversedArray()
ReDim reversedArray(LBound(arrayList, 2) To UBound(arrayList, 2), LBound(arrayList, 1) To UBound(arrayList, 1))

    For iR = LBound(arrayList, 2) To UBound(arrayList, 2)
        For iC = LBound(arrayList, 1) To UBound(arrayList, 1)
        reversedArray(iR, iC) = arrayList(iC, iR)
        Next iC
    Next iR

FUNreversedArray = reversedArray

End Function

Public Function FUNcreateEmail(mailTo, mailCC, mailBCC, mailSubject, mailBody, mailAttachment)
    
    Call FUNsetGFObjects
    Dim OutMail As Object
    Set OutMail = objOutlookApplication.CreateItem(0)

    On Error Resume Next
    With OutMail
        .To = mailTo
        .CC = mailCC
        .BCC = mailBCC
        .Subject = mailSubject
        .Body = mailBody
        If FUNcheckForEmptiness(mailAttachment) = False Then
        For i = LBound(mailAttachment) To UBound(mailAttachment)
        .Attachments.Add (mailAttachment(i))
        Next i
        End If
        .Display
    End With

    On Error GoTo 0
    Set OutMail = Nothing: Set objOutlookApplication = Nothing
End Function

Public Function FUNmailList(Name, isTo) As String

Select Case Name
    Case "PRICING"
        contactname = "Daily Prices"
    Case "PORTFOLIO"
        contactname = "Morningstart Portfolio"
    Case 32, 193, 206
        contactname = "HIQ Bond"
    Case 35, 2405, 2406, 2407, 2408
        contactname = "Sequoia"
    Case 239, 240
        contactname = "Post Opbouw"
    Case 262, 263
        contactname = "Beaumont"
    Case 275, 276, 277
        contactname = "Slim Vermogensbeheer"
    Case 278
        contactname = "Post Helder"
    Case 1606, 1607
        contactname = "Market Portfolio"
    Case 1616, 1617
        contactname = "Stroeve Lemberger"
    Case 2423, 2424
        contactname = "Blauw Tulp"
    Case 34
        contactname = "HIQ FVF"
    Case Else
        contactname = Name
End Select

If isTo = True Then
    contactList = DATget("select if(contact_name='Daily Prices',concat(email_1,email_2),email_1) from counterparty_contact where contact_name='" & contactname & "'", False, FUNswithToProduction("backoffice_p"))
    Else
    contactList = DATget("select if(contact_name='Daily Prices',concat(email_1,email_2),email_2) from counterparty_contact where contact_name='" & contactname & "'", False, FUNswithToProduction("backoffice_p"))
End If

If FUNcheckForEmptiness(contactList) = False Then FUNmailList = contactList(0, 0) Else FUNmailList = "": msg = MsgBox("[Z_GeneralFunction.FUNmailList] Cannot find mail list: " & Name, vbOKOnly + vbInformation)

End Function

Public Function FUNcheckIfFolderPathExists(folderPath)
Call FUNsetGFObjects
If Right(folderPath, 1) <> "\" Then folderPath = folderPath & "\"

If objScriptingFileSystem.FolderExists(folderPath) = False Then
        FUNcheckIfFolderPathExists = False
    Else
        FUNcheckIfFolderPathExists = True
End If

End Function

Public Function FUNconvertToSingleArray(inputArray)

Dim outputArray(): ReDim outputArray(LBound(inputArray, 2) To UBound(inputArray, 2))

For i = LBound(inputArray, 2) To UBound(inputArray, 2)
outputArray(i) = inputArray(0, i)
Next i

FUNconvertToSingleArray = outputArray

End Function

Public Function FUNcheckIfFileNameContainsString(folderPath, stringName)

Call FUNsetGFObjects
Dim objFolder, objFile As Object:
Dim output()
Set objFolder = objScriptingFileSystem.GetFolder(folderPath)

If FUNdimensionCheck(stringName) = 0 Then
    FUNcheckIfFileNameContainsString = False
    For Each objFile In objFolder.Files
            If InStr(objFile.Name, stringName) <> 0 Then
            FUNcheckIfFileNameContainsString = True
            Exit For
        End If
    Next objFile
End If

If FUNdimensionCheck(stringName) = 1 Then
    ReDim output(1 To 2, LBound(stringName) To UBound(stringName))
    For Each objFile In objFolder.Files
        fileName = objFile.Name
        For i = LBound(stringName) To UBound(stringName)
            output(1, i) = stringName(i)
            If InStr(fileName, stringName(i)) <> 0 Then
            output(2, i) = True
            End If
        Next i
    Next objFile

    For i = LBound(output, 2) To UBound(output, 2)
       If IsEmpty(output(2, i)) = True Then output(2, i) = False
    Next i
End If

End Function

Public Function FUNcreateFolderPath(folderPath, Optional ByVal CreatePath As Boolean = False) As String

    Dim s() As String: Dim d As String: Dim i As Integer
    If CreatePath Then
         'If they supplied an ending path seperator, cut it for now
        If Right$(folderPath, 1) = Chr(92) Then _
        folderPath = Left$(folderPath, Len(folderPath) - 1)

        s = Split(folderPath, Chr(92))

        d = s(0)
        For i = 1 To UBound(s)
            d = d & Chr(92) & s(i)
            If Len(Dir(d, vbDirectory)) = 0 Then MkDir d
        Next i
    End If

    If Not Right$(folderPath, 1) = Chr(92) Then _
    FUNcreateFolderPath = folderPath & Chr(92): Exit Function
    FUNcreateFolderPath = folderPath
End Function

Public Function FUNcheckIfFilePathExists(FilePath)

Call FUNsetGFObjects
If objScriptingFileSystem.FileExists(FilePath) = False Then
    FUNcheckIfFilePathExists = False
    Else
    FUNcheckIfFilePathExists = True
End If

End Function


Public Function FUNcreateWordDocument(Location, fileName, fileExtension, table, reportFormat, tableCount, pictureTable, newFile, newPage, closeFile)

    If newFile = True Then
        setObject = FUNsetGFObjects()
        objWordApplication.Visible = True
        Set wrdDoc = objWordApplication.Documents.Add
        Set objRange = wrdDoc.Range
        Else
            '//Set wrdDoc = objWordApplication.ActiveDocument
            Set objRange = wrdDoc.Content
            objRange.Collapse Direction:=0 '//wdCollapseEnd
            wrdDoc.Paragraphs.Add
            Set objRange = wrdDoc.Paragraphs.last.Range

            If newPage = True Then
            objRange.InsertBreak (7) '//wdPageBreak
            End If
    End If

    If FUNcheckForEmptiness(table) = False Then
        wrdDoc.Tables.Add objRange, UBound(table) - LBound(table) + 1, UBound(table, 2) - LBound(table, 2) + 1
        Set objtable1 = wrdDoc.Tables(tableCount)

        For iRow = LBound(table) To UBound(table)
        For iColumn = LBound(table, 2) To UBound(table, 2)
        objtable1.Cell(iRow, iColumn).Range.text = table(iRow, iColumn)
        If InStr(reportFormat(iRow, iColumn), "B") <> 0 Then objtable1.Cell(iRow, iColumn).Range.Font.Bold = True
        If InStr(reportFormat(iRow, iColumn), "I") <> 0 Then objtable1.Cell(iRow, iColumn).Range.Font.Italic = True
        Next iColumn
        Next iRow

        Set objRange = wrdDoc.Content
        objRange.Collapse Direction:=0 '//wdCollapseEnd
        objRange.InsertAfter ""
    End If
    If pictureTable = True Then
    For iRow = LBound(table) To UBound(table)
        For iColumn = LBound(table, 2) To UBound(table, 2)
        If reportFormat(iRow, iColumn) <> "" Then
            If FUNcheckIfFilePathExists(reportFormat(iRow, iColumn)) = True Then
             objtable1.Cell(iRow, iColumn).Range.InlineShapes.AddPicture reportFormat(iRow, iColumn)
            End If
        End If
        Next iColumn
    Next iRow
    objRange.Collapse Direction:=0 '//wdCollapseEnd
    objRange.InsertAfter ""
    End If

    If closeFile = True And fileExtension = ".pdf" Then
    wrdDoc.ExportAsFixedFormat OutputFileName:=Location & fileName & fileExtension, ExportFormat:=17  '//wdExportFormatPDF
    wrdDoc.Close SaveChanges:=0 '//wddonotsavechanges
    objWordApplication.Quit SaveChanges:=0 '//wddonotsavechanges
    End If

End Function

Public Function FUNfolderPathSelector(strPath As String) As String

Dim sItem As String
setObject = FUNsetGFObjects()

With objFileDialog
    .title = "Select a Folder"
    .AllowMultiSelect = False
    .InitialFileName = strPath
    If .Show <> -1 Then GoTo NextCode
    sItem = .SelectedItems(1)
End With
NextCode:

FUNfolderPathSelector = sItem
Set objFileDialog = Nothing

End Function

'Public Function FUNfolderPathSelector(strPath As String) As String
'
'    Dim fldr As FileDialog: Dim sItem As String
'    Set fldr = Application.FileDialog(msoFileDialogFolderPicker)
'    With fldr
'        .Title = "Select a Folder"
'        .AllowMultiSelect = False
'        .InitialFileName = strPath
'        If .Show <> -1 Then GoTo NextCode
'        sItem = .selectedItems(1)
'    End With
'NextCode:
'    FUNfolderPathSelector = sItem
'    Set fldr = Nothing
'
'End Function

Public Function FUNcreateExcelDocument(Location, fileName, fileExtension, newFile, table, closeFile)

    If newFile = True Then
        setObject = FUNsetGFObjects()
        Set objWorkbook = objExcelApplication.Workbooks.Add
    End If
    
    For iRow = LBound(table) To UBound(table)
    For iCol = LBound(table, 2) To UBound(table, 2)
    objExcelApplication.Application.Cells(1, 1).Value = table(iRow, iCol)
    Next
    Next

    If closeFile = True Then
    objWorkbook.SaveAs Location & fileName & fileExtension
    objExcelApplication.Application.Quit
    End If

End Function

Public Function FUNcontrolExists(ctlName, frm)

On Error GoTo ErrHandler
  Dim ctl As Control

  For Each ctl In frm.Controls
    If ctl.Name = ctlName Then
      FUNcontrolExists = True
      Exit For
    End If
  Next ctl

ExitHere:
  Exit Function
ErrHandler:
  Debug.Print Err, Err.Description
  Resume ExitHere
End Function

Public Function FUNcheckFileVersion(fileName)

Dim active As Boolean
Dim activation() As Variant
Dim searchfile As String

activation = DATget("select active from activation where name='" & fileName & "'", False, FUNswithToProduction("backoffice_p"))

If FUNcheckForEmptiness(activation) = False Then
    If activation(0, 0) = 0 Then active = False Else active = True
    Else
    active = False
End If

If active = False Then
    searchfile = Left(fileName, Len(fileName) - 9)

    currentFile = DATget("select name from activation where name like '" & searchfile & "%' and active=1 order by startingdate desc", False, FUNswithToProduction("backoffice_p"))
    If FUNcheckForEmptiness(currentFile) = False Then
    Call MsgBox("This file is no longer active, please request for file:'" & currentFile(0, 0) & "'.", vbOKOnly + vbCritical, "File Not Active"): End
    Else
    Call MsgBox("This file is no longer active, please ask for a new version of the '" & searchfile & "' file.", vbOKOnly + vbCritical, "File Not Active"): End
    End If
End If

End Function

Public Function FUNlimitedChar(text)

FUNlimitedChar = True
For i = 1 To Len(text)
    Select Case Asc(Mid(text, i, 1))
        Case 32, 38, 44, 45, 46, 48 To 57, 65 To 90, 95, 97 To 122
        Case Else
            FUNlimitedChar = False
            Call MsgBox("Please use only following chars: " & vbNewLine & "[ a-z A-Z 0-9 dot space comma " & Chr(38) & " " & Chr(45) & " " & Chr(95) & " ]", vbOKOnly)
            Exit For
    End Select
Next i

End Function

Public Function FUNeuroChar()

Select Case LCase(FUNuserNameWindows())
    Case "mdirks", "mbouwman", "rsnijders", "jwaeghe", "abeyerer", "nneudert"
    FUNeuroChar = Chr(128)
    Case "nstoeva", "gshahinyan", "tarlt", "tbainbridge", "mpetrova", "rtocha"
    FUNeuroChar = Chr(136)
    Case Else
    FUNeuroChar = Chr(128)
End Select

End Function

Public Function FUNcheckRootUser()

root_name = DATget("select user_name from pb_rights where user_name='root';", False, "hiqtrading_t")

If FUNcheckForEmptiness(root_name) = True Then
Dim statement(1 To 1)
statement(1) = "insert into pb_rights values('root',5,7,0);"
    Call DATset(statement, False, "hiqtrading_t")
End If

End Function

Public Function FUNgetFXRates(valuation_date)

sqlRates = DATget("select c.currency_id, alphabetic_code currency_code, round(case alphabetic_code when 'EUR' then 1 when 'DEM' then 1/1.95583 when 'BEF' then  0.024789 when 'NLG' then 1/2.20371 when 'GRD' then 1/340.75 when 'ESP' then 0.00601 when 'PTE' then 0.00499 else price/10000*price_multiplier end,7) fx " & _
            "from currency c left join closeprice on (forex_product_id=product_id and closeprice.date=" & valuation_date & ")" & _
            "left join product using (product_id) having fx is not null;", False, FUNswithToProduction("hiqtrading_p"))

If UBound(sqlRates, 2) > 10 Then
FUNgetFXRates = sqlRates
Else
Call MsgBox("Cannot find fx rates for valuation_date = " & valuation_date, vbOKOnly + vbCritical)
End If

End Function







